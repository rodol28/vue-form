import Vue from "vue";
import Vuex, { Module } from "vuex";

Vue.use(Vuex);

const VEHICLES = [
    {
        "idVehicle": "1",
        "description": "Descripcion del auto FIAT",
        "colour": "#d06f40",
        "brand": "FIAT",
        "model": "Punto",
        "observations": "Observaciones del auto",
        "limit": -1,
        "offset": 0
    },
    {
        "idVehicle": "2",
        "description": "auto azul",
        "colour": "#f76140",
        "brand": "Ford",
        "model": "Fusion",
        "observations": "Ninguna",
        "limit": -1,
        "offset": 0
    }, 
    {
        "idVehicle": "3",
        "description": "Volkwagen Gol Trend rojo",
        "colour": "#a90619",
        "brand": "Volkwagen",
        "model": "Trend",
        "observations": "",
        "limit": -1,
        "offset": 0
    },
    {
        "idVehicle": "4",
        "description": "Fiat Cronos color celeste",
        "colour": "#1884b6",
        "brand": "FIAT",
        "model": "Cronos",
        "observations": "",
        "limit": -1,
        "offset": 0
    }
];

const getDefaultState = () => {
  return {
    pending: 0,
    vehicleList: []
  };
};

// eslint-disable-next-line
export const Vehicle = {
  namespaced: true,
  state: getDefaultState(),
  actions: {
    reload() {
      // console.log("Se llama a reload")
      this.commit("Vehicle/setList", VEHICLES);
    },
  },
  mutations: {
    incPending: (state)  => {
      state.pending++;
    },
    decPending: (state) => {
      state.pending--;
    },
    setList(state, vehicleList) {
      state.vehicleList = vehicleList;
    },
    add(state, item) {
      state.vehicleList.push(item);
    },
    edit(state, item) {
      const index = state.vehicleList.findIndex(
        x => x.idPlateFormat == item.idPlateFormat
      );
      Object.assign(state.vehicleList[index], item);
    },
    remove(state, item) {
      const index = state.vehicleList.findIndex(
        x => x.idPlateFormat == item.idPlateFormat
      );
      state.vehicleList.splice(index, 1);
    }
  },
  getters: {
    loading: (state) => {
      return state.pending !== 0;
    }
  }
};
