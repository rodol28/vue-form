import Vue from "vue";
import Vuex from "vuex";
import { Vehicle } from "./Vehicle.store";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    Vehicle: Vehicle
  },
});
